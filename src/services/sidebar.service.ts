import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SidebarService {
    menuPage: any[] = ['userid','status'];
     
    sidebarVisibilityChange: Subject<string[]> = new Subject<string[]>();

    constructor()  
    {  
        this.sidebarVisibilityChange.subscribe((value) => { 
            this.menuPage = value
        });
    }

    toggleSidebarVisibilty(data) 
    {
        this.sidebarVisibilityChange.next(data);
    }
}