import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
//import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-palert',
  templateUrl: 'palert.html',
})
export class PalertPage {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) 
  {
     this.data = this.navParams.get('data');
     
     localStorage.setItem('popup', "yes");
     setTimeout(()=>{
        this.close();
        localStorage.removeItem('popup');
     },5000);
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad PalertPage');
  }

  close() 
  {
    this.viewCtrl.dismiss({navigate:'no'});
  }

  save()
  {
  	this.viewCtrl.dismiss({data:this.data,navigate:'yes'});
  }
}
