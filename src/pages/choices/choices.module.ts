import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoicesPage } from './choices';

@NgModule({
  declarations: [
    ChoicesPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoicesPage),
  ],
})
export class ChoicesPageModule {}
